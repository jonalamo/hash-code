class Slideshow:
    def __init__(self):
        self.slides = []

    def getScore():
        score = 0

        for i in range(len(self.slides)):   #kill me
            tagsA = set( self.slides[i].getTags )
            tagsB = set( self.slides[i+1].getTags ) 
            
            common = tagsA & tagsB
            differentA = tagsA - tagsB
            differentB = tagsB - tagsA

            interest = min(len(common), len(differentA))
            interest = min(interest, len(differentB))

            score += interest
        
        return score


