from model import Model


class Parser:

    def __init__(self, input_file):
        self.file_line_generator = self.read_file()
        self.parse_input()

        # Structure to be used in model initialization
        self.init_model_structure = {}

    def parse_input(self):

        # Parse first line of input file
        self.parse_heading(next(self.file_line_generator))

        for rest_of_lines in self.file_line_generator:
            # Parse odd lines from file
            self.parse_odd_line(rest_of_lines)
            # Parse pair lines from file (just in case)
            self.parse_pair_line(rest_of_lines)

    def read_file(self, input_file):
        with open(input_file, 'r') as f:
            return (line for line in f.read().splitlines())

    def parse_heading(self, heading_line):
        # Update the init model structure with info from heading
        self.init_model_structure = {}

    def parse_odd_line(self, odd_line):
        # Update the init model structure with info from odd lines
        self.init_model_structure = {}

    def parse_pair_line(self, pair_line):
        # Update the init model structure with info from pair lines
        self.init_model_structure = {}

    def create_model(self):
        # Initialize a Model with the init structure for constraints and problem data...
        return Model(self.init_model_structure)
