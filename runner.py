from slideshow import Slideshow
from slide import Slide
from photo import Photo


if __name__ == "__main__":
    
    #Buscamos la horizontal con mas tags
    horizontalWithMostTags = sqlReturnHorizontalWithMostTags()    #SQL. Convertimos la foto en slide

    #buscamos la combinacion de verticales con mas tags
    #Buscar la vertical con mas tags
    #Buscar otra vertical que tenga mas tags no incluidos en la primera
    verticalWithMostTags = Slide()      #Magic. Sera un slide con 2 verticales

    #cogemos la mayor como punto de partida
    if len( horizontalWithMostTags.getTags() ) > len( verticalWithMostTags.getTags() ):
        firstSlide = horizontalWithMostTags
    else:
        firstSlide = verticalWithMostTags

    #Creamos slideshow con ese slide
    slideshow = Slideshow()
    slideshow.slides.append(firstSlide)


    #Empezamos a iterar!
    done = False
    maximunTagNumDifference = 0
    while not done:
        if maximunTagNumDifference > 10:
            done = True

        #Filtramos restantes horizontales que tengan mismo numero de tags
        #Volvemos a filtrar las que tengan en comun la mitad de tags
        #Cogemos la primera de la lista
        photoWithSameNumTagsAndHalfInCommon = sqlReturnphotoWithSameNumTagsAndHalfInCommon ( photoID, maximunTagNumDifference)

        if photoWithSameNumTagsAndHalfInCommon != None:     #Elegimos la primera foto
            chosenSlide = Slide( [ photoWithSameNumTagsAndHalfInCommon ] )      #la metemos en un slide
            slideshow.slides.append(chosenSlide)        #Metemos el slide en el slideshow

        else:
            #Si la lista de candidatas horizontales esta vacia, vamos a las verticales
            #Cogemos 1 vertical que tenga menos tags que el anterior, y la mitad de coincidencias
            #Buscamos otra vertical que tenga los tags que falten (en numero), y que esos tags solucionen el solape
            chosenVerticalPair = [vPhoto1, vPhoto2]
            if len(chosenVerticalPair) > 0:
                chosenVerticalSlide = Slide(chosenVerticalPair)
                slideshow.slides.append(chosenVerticalSlide)    #Metemos el slide en el slideshow

            else:
                #Horror! No hay ni una foto horizontal que nos mole, ni un par de verticales
                #Tenemos que relajar los constrains
                maximunTagNumDifference += 1
                

    print( slideshow.getScore())