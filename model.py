

class Model:

    def __init__(self, init_parameters):
        self.init_parameters = init_parameters
        self.variables = self.create_variables(init_parameters)
        self.current_objective = 0
        self.step_value = 1

    def create_variables(self, init_parameters):
        """
        Variables dict
        {
            'variable_1': [value, slope, delta],
            'variable_2': [value, slope, delta],
            'variable_3': [value, slope, delta]
        }
        :param init_parameters:
        :return:
        """
        variables = {}
        return variables

    def objective_function(self, variables_set):
        value = 0
        return value

    def check_constraints(self, variables_set):
        """
        Eval all constraints in model and return True or False
        :param variables_set: Dictionary with all variables
        :return: True/False
        """
        result = True
        return result

    def calculate_current_slopes(self):
        """
        For each variable calculate how sensitive is objective function to self.step variations in both directions,
        negative and positive and store the best.
        :return:
        """

        for variable_key in self.variables.keys():

            # Copy current variable set into new dictionary
            new_variable_set = dict(self.variables)
            good_delta = 0
            good_slope = 0

            # Move variable into two directions
            for variable_direction in [1, -1]:
                # Calculate delta for this direction
                delta = variable_direction * self.step_value
                # Increase this delta into new variable set for current variable
                new_variable_set[variable_key][0] += delta

                if self.check_constraints(new_variable_set):
                    # Get objective function result with the new change
                    objective_result = self.objective_function(new_variable_set)
                    # If change is for better, store
                    if objective_result > self.current_objective:
                        good_delta = delta
                        good_slope = objective_result - self.current_objective

                else:
                    good_slope = -1

            # Store the best delta found for this variable into main variables parameter.
            self.variables[variable_key][1] = good_slope
            self.variables[variable_key][2] = good_delta

    def get_best_direction_of_movement(self):
        return max(self.variables, key=lambda x: self.variables[x][1])

    def move_to_best_direction(self):
        variable_key = self.get_best_direction_of_movement()
