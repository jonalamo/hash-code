class Photo:

    def __init__(self, orientation, tags, foto):
    	self.id = foto
        self.orientation = orientation
        self.tags = tags
        
    def merge_verticals(n_tags_foto_anterior, tags):
    	"""
    	"""

    resultado = 0
    id_primera_foto = 0
    id_segunda_foto = 0

    primera_foto = None
    segunda_foto = None

    # calculo de tags optimo
    n_tags_optimo = (n_tags_foto_anterior / 2)

    for i in range(n_tags_foto_anterior):

    	lista_fotos = busqueda.sql("V", i, tags, n_tags_optimo, no_usadas)
    	if lista_fotos:
    		primera_foto = lista_fotos[0]
    	else: 
    		break

    	"""
    	for fotos in lista_fotos:

    		resultado = set(fotos.tags) & set(tags)

    		if len(resultado) <= n_tags_optimo:
    			id_primera_foto = fotos.id
    			primera_foto = fotos
    			break
    		else: 
    			pass
    	"""

    	if len(primera_foto.tags) == n_tags_optimo:
    		# Encontrar una foto que no me sume tags
    		n_tags_pendientes = n_tags_foto_anterior - i 
    		fotos_restantes = busqueda.sql("V", n_tags_pendientes, tags, no_usadas)
    		if fotos_restantes:
    			# Que no esten los tags de la anterior
    			segunda_foto = fotos_restantes[0]
    		else: 
    			break

    	else: 
    		# Encontrar tags pendientes
    		n_tags_pendientes = n_tags_optimo - len(resultado)
    		# Necesito el numero de tags pendientes que coincidan con el valor pasado 
    		fotos_restantes = busqueda.sql("V", n_tags_pendientes, tags, no_usadas)
    		# Nos sirve cualquier valor
    		if fotos_restantes:
    			segunda_foto = fotos_restantes[0]
    		else:
    			break

    return [primera_foto, segunda_foto] 



